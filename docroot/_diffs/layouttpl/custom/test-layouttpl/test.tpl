
 
  <div class="container test_columns" id="main-content" role="main">
	<div class="wd-content-container wd-fix-mt">
		<div class="wd-center">
			<div class="row" id="compRow">						
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<div class="wd-main-content">
						<div class="wd-section portlet-column">
							$processor.processColumn("column-1")
						</div>
						<div class="row row-custom" style="padding-right:0px;margin-top: 6px;">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="portlet-column">									
									$processor.processColumn("column-2")
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-right">
								<div class="portlet-column">									
									$processor.processColumn("column-3")
								</div>
							</div>
						</div>
						<div class="row row-custom">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="portlet-column">									
									$processor.processColumn("column-4")
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
					<div class="wd-right-content portlet-column">
						$processor.processColumn("column-5")
					</div>
				</div>
			</div>
		</div>
	</div>
</div>