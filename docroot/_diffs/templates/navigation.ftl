<div id="wd-menu-desktop" class="hidden-xs hidden-sm">
	<div class="container">
		<div class="row">
			<div class="full-width $nav_css_class" id="navigation">
				<ul class="wd-dropdownMenu">
					<#list nav_items as nav_item>
						<#if nav_item.isSelected()>
							<li class="action">
						<#else>
							<li>
						</#if>
			
							<a href="${nav_item.getURL()}" ${nav_item.getTarget()}><span>${nav_item.icon()} ${nav_item.getName()}</span></a>
			
							<#if nav_item.hasChildren()>
								<ul class="dropdown-menu" role="menu">
									<#list nav_item.getChildren() as nav_child>
										<#if nav_child.isSelected()>
											<li class="action dropdown-submenu" >
										<#else>
											<li class="dropdown-submenu">
										</#if>
			
											<a href="${nav_child.getURL()}" ${nav_child.getTarget()}>${nav_child.getName()}</a>
										</li>
									</#list>
								</ul>
							</#if>
						</li>
					</#list>
				</ul>
			</div>
		</div>	
	</div>
</div>