<!DOCTYPE html>

<#include init />

<html class="<@liferay.language key="lang.dir" />" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
	<title>${the_title} - ${company_name}</title>

	${theme.include(top_head_include)}
	<!-- Bootstrap core CSS -->
 	<link href="/test-theme/css/bootstrap.min.css" rel="stylesheet">
 	<link href="/test-theme/css/custom.css" rel="stylesheet">
</head>

<body class="${css_class}">

${theme.include(body_top_include)}

<#if is_signed_in>
	<@liferay.dockbar />
</#if>

<div>

	<header id="banner" role="banner">	
	
		${theme.runtime('header_WAR_testportlet')}
		
		
		<#if has_navigation || is_signed_in>
			<#include "${full_templates_path}/navigation.ftl" />
		</#if>
	</header>

	<div id="wd-wrapper">		

		<#if selectable>
			${theme.include(content_include)}
		<#else>
			${portletDisplay.recycle()}

			${portletDisplay.setTitle(the_title)}

			${theme.wrapPortlet("portlet.ftl", content_include)}
		</#if>
	</div>

	<footer id="wd-footer-container" role="contentinfo">
		${theme.runtime('footer_WAR_testportlet')}
	</footer>
</div>

${theme.include(body_bottom_include)}

</body>

${theme.include(bottom_include)}

</html>